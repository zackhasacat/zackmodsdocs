Welcome to Lumache's documentation!
===================================

**Zack's Mods** is a set of mods and tools for TES 3: Morrowind.
They primarily target OpenMW, but some use MWSE as well.


Check out the :doc:`usage` section for further information, including
how to :ref:`installation` the project.

.. note::

   This project is under active development.

Contents
--------

.. toctree::

   mods/mundis
   api
